## Q1. What is your one major takeaway from each one of the 6 sections

* Make notes while discussing requirements with your team. Make sure to ask questions and seek clarity in the meeting itself. Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page
* Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel. Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.
* Explain the problem clearly, mention the solutions you tried out to fix the problem. Use screenshots, diagrams, screencasts tools like loom to show and explain.
* Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour. But instead of bombarding them with many messages, you can write down all the questions you have and send it as a single message.
* Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps. You should strongly consider blocking social media sites and apps during work hours. Make sure you manage your food situation well. Too little food or too much food lead to lower levels of concentration. Make sure you do some exercise to keep your energy levels high throughout the day.

## Q2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

I think I need to work on more efficient methods of validating the project requirements that have been written down. I'll write down the requirements on a piece of paper and confirm them in the actual meeting.
Maintaining a positive working relationship with my teammate may enable me to be more productive by resolving problems more quickly. I can attempt to arrange daily meetings for my staff.
