### Ques 1) What are the steps/strategies to do Active Listening?

1. Active listening involves giving your undivided attention to the speaker.
2. It requires focusing on their words and striving to understand their viewpoint.
3. Active listening also entails expressing attentiveness through actions like nodding, maintaining eye contact, and offering appropriate responses.
4. The goal of active listening is to enhance relationships and communication by demonstrating empathy and comprehension.
5. Practice patience: Be patient and avoid rushing the speaker. Give them the necessary time to express their thoughts and ideas fully.
6. Manage internal distractions: Be aware of any internal distractions or biases that might hinder your listening. Stay open-minded and focus on the speaker's message rather than letting your mind wander.

### Que 2) According to Fisher's model, what are the key points of Reflective Listening?

These key points of the fisher model:

1. Reflective Listening aims to grasp the speaker's emotions and feelings. It involves acknowledging and empathizing with their emotional state.
2. A crucial aspect of Reflective Listening is paraphrasing, which means restating or summarizing the speaker's words in your own language. This helps demonstrate that you understand what they are saying.
3. Reflective Listening emphasizes withholding judgment and refraining from jumping to conclusions. It involves creating a safe and non-judgmental space for the speaker to express themselves openly.
4. Reflective Listening requires active engagement, where the listener fully immerses themselves in the conversation. They actively participate by responding appropriately and showing interest.
5. To ensure accurate understanding, Reflective Listening involves seeking clarification when something is unclear. Asking questions and seeking further explanations helps prevent misinterpretation.

### Ques 3) What are the obstacles in your listening process?


1. Distractions: External noises, background music, or even your own thoughts can be a distraction that prevents you from fully concentrating on what someone is saying.
2. Prejudice or bias: Sometimes, we may have preconceived notions about the speaker, their opinions, or their background, which can prevent us from truly listening with an open mind.
3. Technical issues: Sometimes, technical issues such as connectivity problems or server downtime can interfere with my ability to receive and process input.
4. Language barriers: Communication can break down when there is a language barrier between the speaker and listener, leading to misunderstandings or misinterpretations.
5. Differences in communication styles: People have different communication styles, and when these styles clash, it can be challenging to understand each other.

### Que 4) How can I improve my listening skills?

1. Be fully present: Give your undivided attention to the speaker, both physically and mentally, and avoid distractions.
2. refrain from interrupting: Allow the speaker to express their thoughts without interruption, avoiding the urge to interject with your own opinions.
3. Engage in active listening: Show interest and involvement through active listening techniques, such as nodding, maintaining eye contact, and using affirmative gestures.
4. Avoid assumptions: Suspend judgment and refrain from making assumptions about the speaker's message. Listen openly and let their words unfold naturally.
5. seek clarification: If you need clarification, don't hesitate to ask. Summarize or paraphrase what you understood and ask the speaker if your interpretation aligns with their intention.

### Que 5) When do you switch to a Passive communication style in your day-to-day life?

Passive communication is mainly used for keeping the peace, especially when dealing with conflict.
For example:- A man asks in a restaurant for a steak made very, and when the waiter brings it, it is little made. When the waiter asks if everything is to his liking, the man responds affirmatively.

### Que 6) When do you switch to Aggressive communication styles in your day-to-day life?

I don't usually communicate aggressively. But when someone practically refuses to listen to me while I am being assertive then I think it is justified to be aggressive if the matter is important. I remember being aggressive about a year ago when I was trying to discipline my younger brother about something important and he was being unreasonable. The important thing here is to be in control of yourself and not get carried away. Because we are talking about aggressive communication to make unreasonable people listen to us and reason with them, not being aggressive to imply dominance.


### Que 7) When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?

I switch to Passive Aggressive communication - specifically silent treatment - when I am sleepy and tired and the second person refuses to acknowledge that. When feeling sleepy, it is natural to experience a decrease in energy and attentiveness, which can impact effective communication. I think choosing to switch to a more silent mode during these times can be a practical approach to prevent misunderstandings and maintain clarity. By recognizing your fatigue and opting for silence, you can avoid potential miscommunications or errors that could arise from impaired focus or concentration.

### Que 8) How can you make your communication assertive? You can analyse the videos and then think about what steps you can apply in your own life. (Watch the videos first before answering this question.)

1. Recognize and name your feelings: Instead of making judgments about others, focus on expressing your own feelings clearly and honestly. By identifying and naming your emotions, you can communicate them more effectively without blaming or attacking others.
2. Realise your needs: Take the time to understand and articulate your needs. Look beyond the surface level and identify the underlying needs behind your requests. This will help you express your needs more accurately and increase the chances of them being understood and fulfilled.
3. Start with low-stakes situations: Begin practising assertive communication in situations that are less intimidating or have lower stakes. This could involve asking for small favours or making simple requests with people you feel comfortable with. Starting small can help build your confidence and communication skills before tackling more challenging situations.
4. Be aware of your body language: Communication involves more than just words. Pay attention to your body language and tone of voice. Ensure that your non-verbal cues align with your assertive communication style. Maintaining an assertive posture and tone can enhance the impact of your message and convey your confidence.
5. Don't wait to address issues: Address problematic situations or behaviours as early as possible. By speaking up early on, you can prevent negative patterns from becoming ingrained in relationships. Timely communication allows for better resolution and prevents issues from escalating.
6. Assertive communication is a learned skill that takes practice. It may not guarantee that you always get what you want, but it increases the likelihood of having your needs understood and respected in healthy relationships.
