# Tiny Habits

## Tiny Habits - BJ Fogg

## Q1. Your takeaways from the video (Minimum 5 points)

1. For creating long lasting change start with tiny habits.
2. Breaking it down into small steps that are easy to do, even when you don't feel like it.
3. Finding ways to make it enjoyable or rewarding.
4. Finding ways to connect with others who are also trying to make the same change.
5. the key to creating lasting change is to start small and make it easy to stick with your habit. With time     and effort, your tiny habits can add up to big results.



## Tiny Habits by BJ Fogg - Core Message

## Q2. Your takeaways from the video in as much detail as possible

1. Behaviour depends on three factors: motivation, aptitude, and prompt.
2. For simple activities, less motivation is required, and vice versa.
4. A habit can be easily formed, but it can be difficult to keep up over time.
5. The solution is to make the habit as small as possible. Because of this, adopting and establishing it permanently is straightforward.
5. The action prompt is the best way to create new habits. This question harnesses our existing enthusiasm to launch a brand-new, small project.
6. Learning to celebrate after a task is finished is the most crucial part of creating a habit. It keeps the want to repeat the practise alive.

## Q3. How can you use B = MAP to make making new habits easier?

1. James Clear tells us that improving by just 1% is not always noticeable but can be extremely significant in the long run.  
2. The overall concept is that if you can become 1% better every day for 1 year, you’ll end up 37 times better than you were at the beginning of the year.  
3. On the other hand, if you become 1% worse every single day, you will hit rock bottom and end up at ~0 by the end of the year.
4. As you can see, just this tiny bit of math shows just how much these little differences can shape our lives.  
5 It is only after a year or more that we finally understand the overall value of our good habits and the cost of our bad ones.


## Q4. Why it is important to "Shine" or Celebrate after each successful completion of habit?

After every habit is successfully completed, it is crucial to "shine" or celebrate since this reinforces the behaviour and establishes a constructive feedback loop in the brain. Celebrating the accomplishment of a behaviour increases the likelihood that you will carry out that habit in the future because the brain prefers to repeat activities that are connected to satisfying feelings and benefits. Celebrating also fosters a sense of self-worth and self-efficacy, which makes it simpler to take on bigger challenges and objectives. In addition, celebrating can make the habit more joyful, which makes it simpler to keep over time.

## 1% Better Every Day Video

## Q5. Your takeaways from the video (Minimum 5 points)

1. The impacts of habits compound; at first, we don't notice much of a difference, but over time, we notice a big change in ourselves.
2. If you want better results, focus on the stages involved in achieving your objectives rather than just setting them.
3. The best way to change your behaviour is to concentrate on how to achieve your goals rather than what you want to do.
4. If you want to create good habits, make them obvious, appealing, easy, and satisfying.
5. Regular habits are what lead to success, not sudden changes in behaviour. A decision made on a particularly motivational day won't have any effect the next day since you might forget about it.

## Book Summary of Atomic Habits

## Q6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

To form habits, we take a different route. We try to create a habit, and then we pray that after doing this for a while enough, it will stick. We'll incorporate it into our identity.
The identity should take precedence in our thoughts instead. For example, if I want to lose weight and get healthy, I should think of myself as fit and eat properly, refrain from regularly consuming junk food, and exercise frequently.

## Q7. Write about the book's perspective on how to make a good habit easier?

1. To make good habits easier, we must remove as many obstacles as possible from our way.
2. Make habits appealing.
3. Making things simple lowers resistance and prepares our environment for the behaviours we wish to develop. When the friction is decreased, the task is much more likely to be completed.
4. We should try to correlate some sort of instant gratification with our habits in order to develop them into ones that are immediately satisfying.


## Q8. Write about the book's perspective on making a bad habit more difficult?

1. Make it invisible.
2. Make it appear poor.
3. Make it harder.
4. Make it inadequate.

## Reflection

## Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

1. I'm going to start solely consuming wholesome foods going forward.
2. I'll forge an identity for myself as a healthy person. I won't attempt to eat junk food every day. Possibly   a couple of times a month.

## Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?


1. Just before going to bed, I'll put my phone away.
2. I'll tuck the phone into my bag before turning in for the evening. I'd try to read a couple of Kindle ages.
3. Reading some articles regarding the negative effects of using a phone while lying in bed would help me persuade myself.
