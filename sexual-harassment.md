## What kinds of behaviour cause sexual harassment? 

1. Assuming that what one finds acceptable for oneself is also acceptable for others, such as making inappropriate comments or jokes.
2. Engaging in verbal harassment by using offensive language, which can contribute to creating a hostile environment.
3. Disregarding the reception of one's behavior by others, including persistent advances after rejection, which can lead to sexual harassment.
4. Making requests for sexual favors or engaging in non-consensual physical contact, such as touching, pinching, hugging, or kissing.


## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

1. Provide the victim with emotional assistance to ensure they feel supported and avoid feelings of isolation.
2. Try to collect evidence of the incident.
3. Inform your supervisor about the behavior.
4. Seek support from trusted friends and colleagues by reaching out to them.
5. Familiarize yourself with the legal policies in your workplace and evaluate the available procedures and actions you can take to address the situation comfortably.
