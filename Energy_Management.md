### Q1. What are the activities you do that make you relax - Calm quadrant?

Listening to classical music can create a soothing and peaceful atmosphere.
Taking a moment to close my eyes and breathe deeply helps me slow down my heart rate and release tension.
    Going for a walk in nature allows me to connect with the calming beauty of the natural world.
    Taking a relaxing shower with cool water can help me feel refreshed and relieve muscle tension.
    Watching funny videos or cartoons can make me laugh, which releases endorphins and helps me relax.
    Engaging in hobbies or activities that I enjoy, such as drawing, painting, knitting, or playing an instrument, can bring a sense of calm and focus.


## Q2. When do you find getting into the Stress quadrant?

When I am unable to solve some problem.
When I see someone doing anything improperly, I get upset
I become worried when I have a lot of things to complete and a short deadline.
I become nervous when I can't perform a task perfectly.


## Q3. How do you understand if you are in the Excitement quadrant?

I feel a burst of energy and enthusiasm, as if I can't wait to start something.
    My face lights up with a big smile, and I have a twinkle in my eyes.
    I feel a fluttering or tingling sensation in my stomach, like butterflies.
    I find it hard to sit still and have a strong urge to move or jump around.
    I have a racing heart or feel a surge of adrenaline in my body.
    My thoughts are focused on the exciting thing I am about to do, and it's hard to think about anything else.
    I have a positive and optimistic outlook, believing that good things are going to happen.
    I feel like I can do anything or experience excitement while opening gifts.


## Q4. Paraphrase the Sleep is your Superpower video in detail.

Sleep is a crucial component of the learning process, both before and after acquiring new knowledge.
Prior to learning, adequate sleep prepares our brain to process information more efficiently.
Research conducted by the speaker indicates that individuals who obtain sufficient sleep every night are 40% more productive when learning
In contrast, limiting sleep to only 4 hours can lead to a 75% reduction in natural killer cells, which play a critical role in our body's defense against disease.
Establishing a consistent sleep schedule can improve the quality of our sleep. Additionally, maintaining low body and room temperatures can facilitate rapid sleep onset.


## Q5. What are some ideas that you can implement to sleep better?

Follow a consistent sleep schedule by going to bed and waking up at the same time every day, even on weekends.
    Create a cozy and comfortable sleep environment by keeping my bedroom cool, dark, and quiet.
    Avoid using electronic devices, like phones or tablets, before bedtime, as the bright screens can make it harder for me to fall asleep.
    Establish a relaxing bedtime routine, such as reading a book, taking a warm bath, or listening to calming music.
    Make sure to get plenty of physical activity and exercise during the day, as it can help me feel tired and ready for sleep.
    Avoid eating heavy meals or drinking caffeine or sugary drinks close to bedtime, as they can interfere with my sleep.
    Try relaxation techniques, like deep breathing or progressive muscle relaxation, to help my body and mind relax before sleep.
    Consider spending a few minutes meditating before going to bed.


## Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

During a solo kayaking trip, the speaker realized that she was the least skilled member of the group she had joined.
This realization led to a commitment to improve her behavior and overall health.
To achieve her health goals, the speaker began exercising regularly, trying various forms of physical activity such as kickboxing, yoga, and Zumba.
Scientific literature supports that exercise can improve mood, energy levels, memory, and focus by strengthening the prefrontal cortex and hippocampus.
The speaker recommends exercising for at least 30 minutes, four times per week, and mentions that power walking can be a convenient form of exercise that does not require a gym membership.


## Q7. What are some steps you can take to exercise more?

I commit to increasing my physical activity and completing my daily tasks independently, without assistance.
It is recommended to incorporate at least 15 to 20 minutes of physical activity daily.
Opting to walk short distances rather than relying on transportation is a good way to achieve this goal.
Exercising with a friend can provide motivation and enhance the enjoyment of the workout experience.
