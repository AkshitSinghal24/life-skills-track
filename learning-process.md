# Learning Process

## Question 1 : 
### What is the Feynman Technique? Paraphrase the video in your own words.

Feyman technique is used to learn any concept faster by teaching it to someone else in simple language.

There are 4 steps to learn:

1. Take a piece of paper and write the concepts name at the top.
2. explain the concepts to yourself as you are teaching to someone else and use plain english or any language in which you are comfortable with.
3. at this point you have a idea of on which area you want to learn and then go back to the source material to understand them properly.
4. last but not least pinpont any complicated terms and challenge yourself to make them simple for yourself.

## Question 2 : 
### What are the different ways to implement this technique in your learning process?

There are four steps to implementing the Feynman Technique in your learning process:

1.  Choose a concept you want to learn and write it down.
2.  Explain the concept in your own words, as if you were teaching it to someone else.
3.  Identify where you are stopping in your explanation, go back to the source material and learn.
4.  Simplify and use analogies to make the concept easier to understand. 

## Question 3
### Paraphrase the video in detail in your own words.

She discusses effective strategies about how to change our brains thinking for learning and Studying:

1. Explains how our brains work.
2. Helps to create innovative solutions
3. Brains have two modes of thinking
4. Focused Mode - Concentrating on a particular task or problem, easily understand it.
5. Diffuse/Relaxed Mode - Not actively thinking about problem, processing information in the background.

## Question 4
### What are some of the steps that you can take to improve your learning process?

I can take the following steps to improve my learning process :- 

1. Alternating between focused and diffuse thinking with help of breaks.
2. Using different study techniques.   
3. Focus on the learning, not on the outcome
4. Break topics to learn into smaller chunks.
5. Using metaphors and analogies.
6. Solve exercises, revise frequently.
    

## Question 5
### Your key takeaways from the video? Paraphrase your understanding.

The key takeaways are:

1. Break the skill into smaller parts
3. Learn about the skill and what it involves.
4. Focus on the 20 hours: means allotting a time frame to learn the skill.    
5. Remove distractions that could interfere with learning, like social media.
6. Practice regularly. 
7. Get feedback from others to help improve your skill.

## Question 6
### What are some of the steps that you can while approaching a new topic?

There are some steps that we can use while approching a new topic:

1. Divide the topic into smaller subtopics that we can understand one by one.
2. Find reliable sources and effective learning methods for each subtopic, and avoid getting overwhelmed or confused by too much information.
3. Establish a clear and attainable goal for what we want to accomplish by learning the topic and decide how much time we can devote to it.
4. Remove any distractions or obstacles that could disrupt our learning process and motivation.
5. Concentrate on practicing the most critical and pertinent aspects of the topic until we achieve a satisfactory level of competence and confidence.
   By following these steps, we can learn any new topic with more speed and effectiveness.
