# FIREWALL

Firewalls protect our computers from Internet hackers. They can steal our bank details or any other important data from our computers and can reduce our bank balance from thousands of dollars to zero within seconds so a firewall is a must in a computer or in a computer network. In windows and other OS systems like Mac OS firewall is preinstalled.

### There are three types of firewalls:

* Packet filtering Firewalls 
* Application or proxy Firewalls
* Next Generation Firewall (NGFW)

So before starting with firewall types let me explain what data packet. so when we want to download a file of size 200 MB from the internet then we will not receive the entire 200 MB data at once but we will receive a small packet like 5 MB every second some part of this 5 MB packet is occupied with information like which computer is sending the data which computer is receiving the data. the remaining portion of this 5 MB packet contains the part of the actual data that we want to download and this part of the actual data is called payload.

### Packet Filtering Firewall:

When the data packet arrives at a packet filtering firewall it only checks the sender and the receivers IP address and the port number present in the data packet the rule written in the list is called access control list used for data packet verification if everything is OK then the data packet is allowed to pass through packet filtering firewalls and then to your computer. the packet filtering firewall is already present in Internet rooters so they are the cheapest way to implement the only limitation of packet filtering firewall is that internally checks the data portion which is a payload of the data packets so a hacker could send some malicious data tagged in this payload section and packet filtering firewalls provides no security.

### Application / Proxy Firewall:

A proxy service is a network security system that operates at the application layer of the network. Acting as an intermediary between clients and servers, it filters messages and provides an additional layer of protection. Proxy firewalls examine and filter data at the application level, offering security by analyzing the content and behaviour of application-level protocols. They can provide features like caching, content filtering, and intrusion detection, making them more protected networks.

### Next Generation Firewall (NGFW):

Next-Generation Firewalls (NGFWs) are advanced firewalls that do deep packet inspection (DPI) and application-level inspection. With DPI, NGFWs can analyze the contents of packets, enabling them to identify and block all threats. NGFW go beyond traditional firewalls by performing application-level inspection, understanding the context of network traffic, and enforcing protocol-specific rules. NGFWs provide enhanced security features and are designed for this modern era of the internet world.

##### Reference List ->
1. https://www.checkpoint.com/cyber-hub/network-security/what-is-firewall/#:~:text=A%20Firewall%20is%20a%20network,network%20and%20the%20public%20Internet.
2. https://www.geeksforgeeks.org/introduction-of-firewall-in-computer-network/
3. https://www.javatpoint.com/firewall
