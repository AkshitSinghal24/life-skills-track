# Grit and Growth Mindset

## 1. Grit

#### Question 1. Paraphrase (summarize) the video in a few lines. Use your own words.
#### Answer. Grit is perseverance and passion for long-term goals.
1. Talent and luck are not the sole determinants of success; they do not guarantee achievement.
2. Grit is driven by a deep-seated passion for a meaningful goal, influencing one's actions and decisions.
3. Grit involves perseverance in the face of obstacles, mistakes, and slow progress towards the goal.
4. While talent and luck have their place, grit can be equally, if not more, important in the long run.
5. Merely possessing talent does not automatically translate to grit; having clear objectives and unwavering    commitment are vital.
6. Cultivating a growth mindset is essential in fostering grit, particularly in children.
7. A growth mindset entails believing that one's ability to learn and improve is not fixed, but can be         developed through effort and practice.

####  Question 2. What are your key takeaways from the video to take action on?
#### Answer.  These are my key takeaways from the video: 
1. Cultivate self-belief and embrace a growth mindset, trusting in your ability to learn and grow.
2. Define meaningful and clear long-term goals that ignite your passion, and remain dedicated to them.
3. Embrace the idea that your abilities are not fixed, but can be developed through persistent effort and       practice.
4. Demonstrate consistency and perseverance, pushing through challenges and setbacks along the way.
5. Find intrinsic motivation and joy in the work you pursue, as it fuels your determination and drive.
6. View failures and setbacks as valuable opportunities for learning and stepping stones toward success.
7. Acknowledge that progress and improvement require effort, and be willing to put in the necessary work.
8. Stay focused on your goals and maintain a positive mindset, even during times of slow progress or       obstacles.

## 2. Introduction to Growth Mindset

#### Question 3. Paraphrase (summarize) the video in a few lines in your own words.
#### Answer. The summary of this video is:


1. Cultivate self-belief and confidence in your problem-solving abilities, fostering independence.
2. Foster consistency by maintaining a strong belief in your capabilities and persevering in your efforts.
3. Challenge your assumptions and exceed your perceived limitations, pushing yourself to reach new heights.
4. Embrace a long-term plan or curriculum to drive personal growth and development.
5. View setbacks and failures as valuable learning opportunities, recognizing the effort you've invested.
6. Develop a growth mindset, understanding that effort leads to growth and personal success.

#### Question 4. What are your key takeaways from the video to take action on?
#### Answer. Key takeaways from the video are:

1. Embrace the belief that skills and intelligence can be enhanced through deliberate effort and practice.
2. Acknowledge that individuals excel in specific areas due to their dedicated work, contrasting with those     who may not have invested the same level of effort.
3. Recognize your ability to shape and improve your skills through your actions and unwavering commitment.
4. Embrace the understanding that skills are developed progressively over time, rather than being fixed      attributes.
5. Embrace the notion that learning and personal growth are continuous processes throughout one's lifetime.
6. Value and appreciate the significance of investing effort, as it serves as a crucial catalyst for achieving success.
7. Approach challenges with an open mind, viewing them as opportunities for personal advancement.
8. Embrace the wisdom gained from past mistakes and failures, utilizing them as invaluable guides for future improvement and growth.
    
## 3. Understanding Internal Locus of Control

#### Question 5. What is the Internal Locus of Control? What is the key point in the video?
#### Answer.  Internal locus of control means:

1. Believing that the outcome of your efforts is the result of your hard work and extra efforts.
2. It is the belief that you have complete control over how much you put into something.

#### The key takeaways are:
1. Changing from an external locus of control to an internal locus of control requires actively working on improving things.
2. Taking responsibility for your choices and efforts empowers you to have a greater sense of motivation and self-belief.
3. Believing in your ability to influence outcomes can lead to a proactive mindset.
4. Having an internal locus of control helps individuals feel more in control of their lives.
5. It encourages individuals to take initiative and make positive changes.
6. Developing an internal locus of control involves recognizing that you have the power to shape your future through your actions and choices.

## 4. How to Build a Growth Mindset

#### Question 6. Paraphrase (summarize) the video in a few lines in your own words.
#### Answer. The summary of the video:
1. Believe in your ability to figure things out and overcome challenges.
2. Question your assumptions and don't let them limit your future possibilities.
3. Develop a life curriculum by actively seeking opportunities for growth and learning.
4. Embrace and honor the struggles and difficulties you encounter as they contribute to your personal growth.
5. View challenges and setbacks as opportunities for learning and improvement.
6. Cultivate a belief that skills and abilities can be developed through effort and persistence.
7. Seek feedback from others to gain valuable insights and improve yourself.
8. Stay open-minded, be willing to try new things, and maintain a positive attitude toward learning and      growth.

#### Question 7. What are your key takeaways from the video to take action on?
#### Answer. Key takeaways from the video are:
1. Have faith in your capacity to problem-solve and discover solutions.
2. Question your assumptions to broaden your perspectives and expand your possibilities.
3. Develop a personalized roadmap or strategy to progress towards your long-term objectives.
4. mbrace failures as valuable learning opportunities rather than sources of fear or discouragement.
5. Recognize that difficulties and challenges are integral to personal growth and maturation.
6. Dedicate yourself to diligent effort and persistence in honing your skills and capabilities.
7. Seek constructive feedback from others to gain valuable insights and enhance your progress.
8. Maintain an open-minded approach, be willing to explore new avenues, and nurture a positive mindset     towards continual learning and development.

## 5. Mindset - A MountBlue Warrior Reference Manual

#### Question 8. What are one or more points that you want to take action on from the manual? (Maximum 3)
#### Answer. I want to take action on these points:
1. I will stay relaxed and focused no matter what happens.
2. I know more efforts lead to better understanding.
3. I will follow the steps required to solve problems:
  * Relax    
  * Focus - What is the problem? What do I need to know to solve it?
  * Understand - Documentation, Google, Stack Overflow, GitHub Issues, Internet    
  * Code  
  * Repeat

